package spring_app10_anno;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;

public class TestMain {
    public static void main(String[] args) {
        
        ApplicationContext ctx = new  GenericXmlApplicationContext("classpath:xml/app1.xml");
        // ClassPathXmlApplicationContext("xml/app1.xml") 패키지명을 포함하거나
        // ClassPathXmlApplicationContext("app1.xml", Test.class) Test.class 와 같은곳에 있다는걸 명시해줌
        // GenericXmlApplicationContext("classpath:xml/app1.xml")
        
        Monitor m = ctx.getBean("monitor", Monitor.class);
        m.showMonitor();
    }
}
